package com.phenrique.addresscontrol;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.phenrique.addresscontrol.helper.CorreiosWS;
import com.phenrique.addresscontrol.model.Address;

@RunWith(SpringJUnit4ClassRunner.class)
public class CorreiosWsTest {

	@Test
	public void findAddressByZipCodeTest() throws Exception {
		Address addr = CorreiosWS.findAddressByZipCode("05107111");
		assertThat(addr, hasProperty("cep", equalTo("05107000")));
		assertThat(addr, hasProperty("end"));
		assertThat(addr, hasProperty("bairro"));
		assertThat(addr, hasProperty("cidade"));
		assertThat(addr, hasProperty("uf"));
		assertThat(addr, hasProperty("complemento"));
		assertThat(addr, hasProperty("complemento2"));

	}

	@Test(expected = IOException.class)
	public void ioExceptionTest() throws Exception {
		CorreiosWS.findAddressByZipCode("07700000");
	}

	@Test(expected = StringIndexOutOfBoundsException.class)
	public void outOfBoundExceptionTest() throws Exception {
		CorreiosWS.findAddressByZipCode("077000005345");
	}
}
