package com.phenrique.addresscontrol;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.phenrique.addresscontrol.application.Application;
import com.phenrique.addresscontrol.controller.AddressController;
import com.phenrique.addresscontrol.model.Address;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class AddressControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AddressController addressController;

	private MockMvc mockMvcStandalone = MockMvcBuilders.standaloneSetup(new AddressController()).build();

	public Address address;

	@Before
	public void initializeEntity() {
		address = new Address(1L, "05107000", "Avenida dos Remédios", "376", "Jardim Belaura", "São Paulo", "SP", null,
				null);
	}

	@Test
	public void searchAddressFromCorreiosReplacingZerosWithSuccess() throws Exception {

		Address address = new Address(null, "05107111", null, null, null, null, null, null, null);

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		mockMvcStandalone
				.perform(post("/api/address-from-correios").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk()).andExpect(jsonPath("$.cep").value("05107000")).andDo(print());
	}

	@Test
	public void addAddressWithSucess() throws Exception {

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		mockMvc.perform(post("/api/save-update").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk()).andDo(print());

	}
	
	@Test
	public void findById() throws Exception {

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		mockMvc.perform(post("/api/save-update").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());

		mockMvc.perform(post("/api/find-by-id").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void findByCep() throws Exception {

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		mockMvc.perform(post("/api/save-update").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());

		mockMvc.perform(post("/api/find-by-cep").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void delete() throws Exception {

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		mockMvc.perform(post("/api/save-update").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());

		mockMvc.perform(post("/api/remove").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void findAll() throws Exception {

		Gson gson = new Gson();
		String json = gson.toJson(address, Address.class);

		for (int i = 0; i > 10; i++) {
			mockMvc.perform(post("/api/save-update").contentType(MediaType.APPLICATION_JSON).content(json))
					.andExpect(status().isOk());
		}

		mockMvc.perform(post("/api/list").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andDo(print());
	}

}
