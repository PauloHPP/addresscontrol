package com.phenrique.addresscontrol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.phenrique.addresscontrol.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

	public List<Address> findAll();

	public Address findById(Long id);

	public List<Address> findByCep(String cep);
}
