package com.phenrique.addresscontrol.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.phenrique.addresscontrol.controller.AddressController;

@SpringBootApplication
@ComponentScan(basePackageClasses = AddressController.class)
@EntityScan("com.phenrique.addresscontrol.model")
@EnableJpaRepositories("com.phenrique.addresscontrol.repository")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
