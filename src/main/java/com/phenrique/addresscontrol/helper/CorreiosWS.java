package com.phenrique.addresscontrol.helper;

import java.io.IOException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.phenrique.addresscontrol.model.Address;

public class CorreiosWS {

	private CorreiosWS() {
	}

	private static final String SIGESP_CLIENT_ADDRESS = "http://cliente.bean.master.sigep.bsb.correios.com.br/";
	private static final String CORREIOS_WS_ADDRESS = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl";

	public static Address findAddressByZipCode(String zipCode) throws Exception {

		Address address = new Address();

		try {
			NodeList returnNodes = callCorreios(zipCode);
			String newZip = zipCode;

			for (int i = 0; i <= zipCode.length(); i++) {
				if (newZip.equals("00000000")) {
					throw new IOException("CEP invalido");
				}
				returnNodes = callCorreios(newZip);

				if (returnNodes.getLength() > 0) {
					Node returnNode = returnNodes.item(0);
					for (Node node : Utility.iterable(returnNode.getChildNodes())) {
						Utility.setByReflection(address, node.getNodeName(), !node.getNodeName().equalsIgnoreCase("id") ? node.getTextContent() : null);
					}
					break;
				} else {
					newZip = Utility.replaceLastNonZero(newZip);
				}
			}

		} catch (SOAPException e) {
			throw new SOAPException("Erro de conexao - " + e.getMessage());
		}

		return address;
	}

	private static NodeList callCorreios(String zipCode) throws SOAPException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage msg = messageFactory.createMessage();
		SOAPEnvelope env = msg.getSOAPPart().getEnvelope();

		env.addNamespaceDeclaration("client", SIGESP_CLIENT_ADDRESS);
		SOAPElement be = env.getBody().addChildElement(env.createName("client:consultaCEP"));
		be.addChildElement("cep").addTextNode(zipCode);
		msg.saveChanges();

		SOAPConnectionFactory connFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection conn = connFactory.createConnection();
		SOAPMessage rp = conn.call(msg, CORREIOS_WS_ADDRESS);

		NodeList returnNodes = rp.getSOAPBody().getElementsByTagName("return");
		conn.close();
		return returnNodes;
	}

}