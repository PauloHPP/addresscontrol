package com.phenrique.addresscontrol.helper;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Utility {

	private Utility() {
	}

	public static Iterable<Node> iterable(final NodeList n) {
		return new Iterable<Node>() {
			@Override
			public Iterator<Node> iterator() {
				return new Iterator<Node>() {
					int index = 0;

					@Override
					public boolean hasNext() {
						return index < n.getLength();
					}

					@Override
					public Node next() {
						if (hasNext()) {
							return n.item(index++);
						} else {
							throw new NoSuchElementException();
						}
					}

					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	@SuppressWarnings("unchecked")
	public static <T> T getByReflection(Object object, String fieldName) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				Field field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				return (T) field.get(object);
			} catch (NoSuchFieldException e) {
				clazz = clazz.getSuperclass();
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		return null;
	}

	public static boolean setByReflection(Object object, String fieldName, Object fieldValue) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				Field field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(object, fieldValue);
				return true;
			} catch (NoSuchFieldException e) {
				clazz = clazz.getSuperclass();
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		return false;
	}

	public static String replaceLastNonZero(String cep) {
		if (!cep.endsWith("0")) {
			String newCep = cep.substring(0, cep.length() - 1) + "0";
			return newCep;
		} else {
			int i = 1;
			while (cep.charAt(cep.lastIndexOf("0") - i) == '0') {
				i++;
			}
			char[] chars = cep.toCharArray();
			chars[cep.lastIndexOf("0") - i] = '0';
			return String.valueOf(chars);
		}
	}

}
