package com.phenrique.addresscontrol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(nullable = false)
	private String cep;

	@NotNull
	@Column(nullable = false)
	private String end;

	@NotNull
	@Column(nullable = false)
	private String numero;

	private String bairro;

	@NotNull
	@Column(nullable = false)
	private String cidade;

	@NotNull
	@Column(nullable = false)
	private String uf;

	private String complemento;

	private String complemento2;

	public Address() {
	}

	public Address(Long id, String cep, String end, String numero, String bairro, String cidade, String uf,
			String complemento, String complemento2) {
		super();
		this.id = id;
		this.cep = cep;
		this.end = end;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
		this.uf = uf;
		this.complemento = complemento;
		this.complemento2 = complemento2;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getComplemento2() {
		return complemento2;
	}

	public void setComplemento2(String complemento2) {
		this.complemento2 = complemento2;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", cep=" + cep + ", end=" + end + ", numero=" + numero + ", bairro=" + bairro
				+ ", cidade=" + cidade + ", uf=" + uf + ", complemento=" + complemento + ", complemento2="
				+ complemento2 + "]";
	}

}