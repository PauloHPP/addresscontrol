package com.phenrique.addresscontrol.controller;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.SOAPException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.phenrique.addresscontrol.model.ExceptionJSONInfo;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { Exception.class, IOException.class, SOAPException.class, PersistenceException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ExceptionJSONInfo handleNotFoundException(HttpServletRequest request, Exception ex) {

		ExceptionJSONInfo response = new ExceptionJSONInfo();
		response.setUrl(request.getRequestURL().toString());
		response.setMessage(ex.getMessage());

		return response;
	}

}
