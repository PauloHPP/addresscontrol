package com.phenrique.addresscontrol.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.phenrique.addresscontrol.helper.CorreiosWS;
import com.phenrique.addresscontrol.model.Address;
import com.phenrique.addresscontrol.repository.AddressRepository;

@RestController
@RequestMapping("/api")
public class AddressController {

	@Autowired
	private AddressRepository repository;

	@RequestMapping(method = RequestMethod.POST, value = "/address-from-correios")
	public @ResponseBody ResponseEntity<?> findOnCorreiosByCep(@RequestBody Address address) throws Exception {
		Address addressFound = CorreiosWS.findAddressByZipCode(address.getCep());
		return ResponseEntity.ok(addressFound);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-update")
	public @ResponseBody ResponseEntity<?> addOrUpdate(@RequestBody Address address) {
		Address saved = repository.save(address);
		return ResponseEntity.ok(saved);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/remove")
	public void removeAddress(@RequestBody Address address) {
		repository.delete(address.getId());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/find-by-id")
	public @ResponseBody ResponseEntity<?> findAddressById(@RequestBody Address address) {
		Address found = repository.findById(address.getId());
		return ResponseEntity.ok(found);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/find-by-cep")
	public @ResponseBody ResponseEntity<?> findAddressByCep(@RequestBody Address address) {
		List<Address> found = repository.findByCep(address.getCep());
		return ResponseEntity.ok(found);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/list")
	public @ResponseBody ResponseEntity<?> list() {
		return ResponseEntity.ok(repository.findAll());
	}
}
