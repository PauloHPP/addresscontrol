#Aplicação RESTful para cadastro de Endereços#

A aplicação possui integração com o WebService dos Correios e consulta o endereço inicialmente através do mesmo, com os dados retornados, é possível manter um cadastro de endereços em uma base de dados local.

###Stack de Tecnologias###

- Java 8
- Spring Boot: Web, Test, Data JPA e Maven Plugin
- Camada de testes com JUnit, Mockito, MockMVC e Hamcrest
- Google GSON para fazer parse de teste
- Uso de aplicação Postman para testar uso
- Banco de Dados em memória: HSQLDB
- Java SOAP, para comunicação com web service dos Correios

###Guia de uso do serviço###

Todos os serviços estão em `[SERVERHOST]:[PORTA]/api`

- `/address-from-correios` - é usado para encontrar o endereço no web service dos Correios, é necessário fazer a requisição enviando o JSON: `{"cep":"[numeroDoCep]"}`

- `/save-update` - é usado para incluir um novo endereço ou alterar um endereço existente. Os parâmetros com o valor `null`, no exemplo, não são obrigatórios. Para atualizar um cadastro existente, o parâmetro id precisa estar preenchido com o id do endereço a ser editado. Ao fazer a requisição é preciso enviar o JSON:
```
{
    "id": [null - para novo / idDoEndereco - para edição],
    "cep": "numeroDoCep",
    "end": "logradouro",
    "bairro": "bairro",
    "cidade": "cidade",
    "uf": "codigoDoEstado",
    "numero": "numero",
    "complemento":  null,
    "complemento2": null
}
```

- `/remove` - é usado para deletar um endereço, a exclusão é feita utilizando o parametro id, sendo assim o JSON enviado na requisição precisa apenas conter `{"id":"[idDoEndereco]"}`

- `/find-by-id` - para encontrar um endereço através do id, útil em caso de edição em single page application client, assim a requisição carrega o formulário com os dados mais atuais do objeto, o JSON enviado na requisição é: `{"id":"[idDoEndereco]"}`

- `/find-by-cep` - em caso de busca no cadastro, a requisição pode ser utilizada como filtro, a requisição exige um JSON com este formato: `{"cep":"[numeroDoCep]"}`

- `/list` - lista todos os dados cadastrados, não exige JSON no corpo da requisição

###Setup da Aplicação###

É necessário ter instalado:

- JDK 1.8 ou versão acima
- Maven 3.0 ou versão acima

- Para fazer o build e deploy da aplicação diretamente, execute o comando `./mvn spring-boot:run`
- Para fazer o build da aplicação `./mvn clean package` e executar o arquivo JAR gerado `java -jar target/address-control-0.1.0.jar`

###Métodos Úteis###

A classe `Utility` foi criada para manter métodos que auxiliam no funcionamento do WebService

- Para iterar os dados recebidos do WebService dos Correios o método `Iterable<Node> iterable(final NodeList n)` visto que `Node` e `NodeList` não implementam `Iterable`
- Para setar os dados de forma dinâmica foi criado o método `setByReflection(Object object, String fieldName, Object fieldValue)` que  utiliza reflection
- O método `replaceLastNonZero(String cep)` substitui os números do CEP de trás para frente de forma a procurar um CEP válido no WebService dos Correios

###Testes###

- `ApplicationTest` verifica o funcionamento da classe `Application` que implementa as annotations do Spring Boot
- `CorreiosWsTest` testa o funcionamento das classes que que chamam o serviço dos Correios
- `AddressControllerTest` verifica o funcionamento de todos os serviços expostos pela API